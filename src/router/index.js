import Vue from 'vue'
import Router from 'vue-router'

import Route from '@/components/Route'
// import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import Register from '@/components/Register'

import User from '@/components/User'
import Admin from '@/components/Admin'
import Log from '@/components/Log'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/Log/TopUp',
      name: 'LogTopUp',
      component: Log.TopUp
    },
    {
      path: '/',
      component: Login
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/Route',
      name: 'Route',
      component: Route
    },
    {
      path: '/Admin/Accounts',
      name: 'Accounts',
      component: Admin.Accounts
    },
    {
      path: '/Admin/Announce',
      name: 'Announce',
      component: Admin.Announce
    },
    {
      path: '/Admin/InvoiceList',
      name: 'InvoiceList',
      component: Admin.Invoice
    },
    {
      path: '/Admin/Refill',
      name: 'RefillList',
      component: Admin.Refill
    },
    {
      path: '/Admin/Transport',
      name: 'Transport',
      component: Admin.Transport
    },
    {
      path: '/User/Profile',
      name: 'Profile',
      component: User.Profile
    },
    {
      path: '/User/Profile/:memCode',
      name: 'ProfileUser',
      component: User.Profile
    },
    {
      path: '/User/RegisterTracking',
      name: 'RegisterTracking',
      component: User.RegisterTracking
    },
    {
      path: '/User/UpdateTracking',
      name: 'UpdateTracking',
      component: User.UpdateTracking
    },
    {
      path: '/User/Tracking',
      name: 'Tracking',
      component: User.Tracking
    },
    {
      path: '/User/Export',
      name: 'Export',
      component: User.Export
    },
    {
      path: '/User/Delivery',
      name: 'Delivery',
      component: User.Delivery
    },
    {
      path: '/User/History',
      name: 'History',
      component: User.History
    },
    {
      path: '/User/Payment',
      name: 'Payment',
      component: User.Payment
    },
    {
      path: '/User/TopUp',
      name: 'TopUp',
      component: User.TopUp
    },
    {
      path: '/User/HistoryTopUp',
      name: 'HistoryTopUp',
      component: User.HistoryTopUp
    },
    {
      path: '/User/UpdateHistory',
      name: 'UpdateHistory',
      component: User.UpdateHistory
    }
  ]
})
