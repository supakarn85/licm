import Profile from './Profile'
import RegisterTracking from './RegisterTracking'
import UpdateTracking from './UpdateTracking'
import Tracking from './Tracking'
import Export from './Export'
import Delivery from './Delivery'
import TopUp from './TopUp'
import HistoryTopUp from './HistoryTopUp'
import Payment from './Payment'
import History from './History'
import UpdateHistory from './UpdateHistory'

export default {
  // Accounts,
  Profile,
  RegisterTracking,
  UpdateTracking,
  UpdateHistory,
  Tracking,
  Delivery,
  TopUp,
  HistoryTopUp,
  Payment,
  Export,
  History
}
