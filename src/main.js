// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueSweetalert2 from 'vue-sweetalert2'
import VueSession from 'vue-session'

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import { sha256 } from 'js-sha256'
// import AxiosPlugin from 'vue-axios-cors'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'sweetalert2/dist/sweetalert2.min.css'
import 'animate.css/animate.min.css'

library.add(fas)

Vue.component('f-icon', FontAwesomeIcon)
Vue.use(VueSweetalert2)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueSession)
// Vue.use(AxiosPlugin)

Vue.config.productionTip = false

Vue.mixin({
  data () {
    return {
      headerAPI: {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Methods': "GET, POST, PATCH, PUT, DELETE, OPTIONS",
        'Access-Control-Allow-Headers': "Origin, Content-Type, X-Auth-Token",
        'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8",
        'Accept': "application/json",
        'application-key': 'T2FLZ#Xo:!WJ%qnC+jPf(U&EkHDi_K8V*S(QKb)!Rcqd6XI8pOyl_unZWN2PS,E*1r=f*eL7E:4%;6y@I.)$!P_xHCnh,q3AKU#tap',
        // 'Content-Type': 'application/json'
      },
      footerLC: '© 2021 LICM Cargo All rights reserved.',
      logoName: 'LICM Cargo',
      logoImg: 'https://firebasestorage.googleapis.com/v0/b/trackingpreorder.appspot.com/o/LICMcargoLogo.jpg?alt=media&token=27186f27-5245-4e2e-bad8-47ccb1ab2a8a'
    }
  },
  methods: {
    getNowGlobal () {
      const today = new Date()

      const month = (today.getMonth() + 1) > 9 ? (today.getMonth() + 1) : '0' + (today.getMonth() + 1)
      const day = today.getDate() > 9 ? today.getDate() : '0' + today.getDate()

      const hour = today.getHours() > 9 ? today.getHours() : '0' + today.getHours()
      // const hour = today.getUTCHours() > 9 ? today.getUTCHours() : '0' + today.getUTCHours()
      const minute = today.getMinutes() > 9 ? today.getMinutes() : '0' + today.getMinutes()

      const date =
        today.getFullYear() +
        '-' + month +
        '-' + day
      const time =
        hour + ':' + minute
        // ':' +
        // today.getSeconds() +
        // ':' +
        // today.getMilliseconds()
      const dateTime = date + ' ' + time
      return dateTime
    },
    getDataStorageUser () {
      var dataUser = {}
      // var memberStorage = localStorage.getItem('memberData')
      let memberSession = this.$session.get('mbd')
      let dataTime = new Date().getTime()

      if (memberSession) {
        dataUser = JSON.parse(memberSession)
        // console.log("User timeout :", new Date(dataUser.timeout))

        if (dataUser.timeout > dataTime) {
          dataUser.timeout = dataTime + 900000
          // localStorage.setItem('memberData', JSON.stringify(dataUser))
          this.$session.set('mbd', JSON.stringify(dataUser))

          return dataUser
        } else {
          this.$swal("หมดเวลา","หมดเวลาเข้าระบบ กรุณาเข้าระบบใหม่","error")
          return null
        }

      } else {
        return null
      }
    },
    formatDateTime (dtDate) {
      if (dtDate && dtDate !== '0000-00-00 00:00:00') {
        var arrayDateTime = dtDate.split(' ')
        var strDateShort = arrayDateTime[0].split('-')[2] + '/' + arrayDateTime[0].split('-')[1] + '/' + arrayDateTime[0].split('-')[0].substring(2, 4)
        return {
          date: strDateShort,
          time: arrayDateTime[1]
        }
      } else {
        return {
          date: '-',
          time: '-'
        }
      }
    },
    getHeaderAPI (nameAPI, userName = '', id = '') {
      // sha256
      let hashKey = ''

      if (userName) {
        hashKey = sha256(hashKey + userName)
      }

      if (id) {
        hashKey = sha256(hashKey + id)
      }

      hashKey = sha256(hashKey + 'You_can_not_in_' + nameAPI + '!!!')
      console.log(nameAPI,hashKey)

      let headerAPI = Object.assign({}, this.headerAPI)
      headerAPI['application-key'] = this.headerAPI['application-key'] + hashKey

      // if (nameAPI === 'Login'){
      return headerAPI
      // } else {
      //   return this.headerAPI
      // }
    },
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
